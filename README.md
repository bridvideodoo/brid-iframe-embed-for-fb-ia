# Brid iframe embed code for Facebook Instant Articles

Maximize your ad revenue by sharing your domain URL through Brid player Facebook Instant Article embed codes.

# Minimal setup
Will embed your player as configured in Brid.

<figure class="op-interactive"> 
<iframe src="//yourdomain.com/brid.php?id=PLAYER_ID&video=VIDEO_ID&partner_id=PARTNER_ID" allowfullscreen width="480" height="270" frameborder="0" ></iframe> 
</figure>


# Override Autoplay
Override your player's autoplay settings by adding the autoplay param to the iframe source as below.

<figure class="op-interactive"> 
<iframe src="//yourdomain.com/brid.php?id=PLAYER_ID&video=VIDEO_ID&partner_id=PARTNER_ID&autoplay=1" allowfullscreen width="480" height="270" frameborder="0" ></iframe> 
</figure>
