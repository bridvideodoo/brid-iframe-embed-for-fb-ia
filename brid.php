<?php
/**
 * 
 * Facebook Instant articles iframe generator.
 *
 * This file is part of Brid.tv project.
 *
 * @version  	1.0.1
 * @since 		1.0.1
 * @author  	BridTv Dev. Team <support@brid.tv>
 * @copyright 	Copyright (c) 2012-2017 Brid Video DOO (https://www.brid.tv)
 * This script cannot be copied and/or distributed without the express permission from Brid.tv.
 */
define('BRID_FB_IA_EMBED_VER', '1.0.1');
header('X-Brid-Fbia-Ver: '.BRID_FB_IA_EMBED_VER);
try{
	/**
	 * Check minimum PHP version required
	 */
	if (version_compare(PHP_VERSION, '5.3.0','<')) {
	    throw new Exception('PHP 5.3.0 is required.');
	}
	/**
	 * Check is json_encode exist
	 */
	if(!function_exists('json_encode')){
		throw new Exception('Json encode function is missing.');
	}
	/**
	 * Allowed parameteres sent via GET
	 */
	$allowed = array(
					'id' =>array('type'=>'string'),
					'video'=>array('type'=>'string'),
					'playlist'=>array('type'=>'string'),
					'width'=>array('type'=>'string'),
					'height'=>array('type'=>'string'),
					'title'=>array('type'=>'string'),
					'playButtonColor'=>array('type'=>'string'),
					'titleBar'=>array('type'=>'bool'),
					'slide_inview'=>array('type'=>'string'),
					'monetize'=>array('type'=>'bool'),
					'autoplay'=>array('type'=>'bool'),
					'autoplay_desktop'=>array('type'=>'bool'),
					'autoplay_on_ad'=>array('type'=>'bool'),
					'autoplayInview'=>array('type'=>'bool'),
					'slide_inview_show'=>array('type'=>'string'),
					'slide_inview_seconds'=>array('type'=>'string'),
					'share'=>array('type'=>'string'),
					'volume'=>array('type'=>'bool'),
					'fullscreen'=>array('type'=>'bool'),
					'controls'=>array('type'=>'bool'),
					'controlsBehavior'=>array('type'=>'string'),
					'videoPlayback'=>array('type'=>'string'),
					'playlistPlayback'=>array('type'=>'string'),
					'afterPlaylistEnd'=>array('type'=>'string'),
					'voipSocEnabled'=>array('type'=>'bool'),
					'comscore'=>array('type'=>'string'),
					'veepsChat'=>array('type'=>'bool'),
					'pauseOffView'=>array('type'=>'bool'),
					'start_volume'=>array('type'=>'int'),
					'hover_volume'=>array('type'=>'int'),
					'play_in_page'=>array('type'=>'bool'),
					'inpage_mobile'=>array('type'=>'bool'),
					'playlistScreen'=>array('type'=>'bool'),
					'shareScreen'=>array('type'=>'bool'),
					'primaryColor'=>array('type'=>'string'),
					'adSDK'=>array('type'=>'array'),
					'partner_id'=>array('type'=>'string'),
					'Ad'=>array('type'=>'object'),
					'meta'=>array('type'=>'bool'),
				);
	$meta = false;
	/**
	  * Required parameters
	  */
	$required = array('id', 'partner_id');
	/**
	  * Page title
	  */
	$pageTitle = '';
	/**
	  * Brid unique id
	  */
	$unique_id = 'Brid_'.substr(md5(time()), 6);
	/**
	  * Parnter id
	  */
	$partner_id = null;
	/**
	 * Default options array
	 */
	$options = array('id'=>null, 'width'=>'100%', 'height'=>'100%');
	if(isset($_GET)){
		$options = array_merge($options, $_GET);
	}
	/**
	 * Sanitize
	 */
	foreach($options as $k=>$v){
		if(!in_array($k, array_keys($allowed))){
			unset($options[$k]);
		}
	}
	/**
	 * Unset title from options
	 */
	if(isset($options['title'])){
		$pageTitle = strip_tags($options['title']);
		unset($options['title']);
	}
	/**
	 * Check minimum required fields
	 */
	foreach($required as $k=>$v){
		if(!isset($options[$v])){
			throw new Exception('Required parameter: "'.$v.'" is missing.');
		}
	}

	/**
	 * Check for video OR playlist
	 */
	if( !isset( $options['video'] ) && !isset( $options['playlist']) ) {
		throw new Exception('URL must either have video or playlist parameter');
	}

	/**
	 * Uunset pid from options
	 */
	if(isset($options['partner_id'])){
		$partner_id = intval($options['partner_id']);
		unset($options['partner_id']);
	}
	/**
	 * Unset meta from options
	 */
	if(isset($options['meta'])){
		$meta = intval($options['meta']);
		unset($options['meta']);
	}
	/**
	 * Convert types
	 */
	foreach($allowed as $k=>$v){
		if(isset($options[$k]) && isset($v['type'])){

			switch($v['type']){
				case 'bool':
					$options[$k] = $options[$k] == true ? true : false;
					break;
				case 'int':
					$options[$k] = intval($options[$k]);
					break;
				case 'array':
				case 'object':
					$options[$k] = json_decode($options[$k]);
					break;
				case 'string':
				default:
					$options[$k] = strip_tags($options[$k]);
					break;
			}
			
		}
	}


?>
<html style="height:100%">
<head>
<?php if(!empty($pageTitle)) { ?>
<title><?php echo $pageTitle; ?></title>
<?php } ?>
<script type="text/javascript" src="//services.brid.tv/player/build/brid.min.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
<?php if($meta) { $current_url = "//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
<meta itemprop="name" content="<?php echo $pageTitle; ?>">
<meta itemprop="thumbnailUrl" content="//cdn.brid.tv/live/partners/<?php echo $partner_id; ?>/snapshot/<?php echo $options['video']; ?>/00001.png">
<meta itemprop="contentURL" content="//cdn.brid.tv/live/partners/<?php echo $partner_id; ?>/sd/<?php echo $options['video']; ?>.mp4">
<meta itemprop="embedURL" content="<?php echo $current_url; ?>">
<?php } ?>
</head>
<body style="margin:0px;padding:0px;height:100%;overflow:hidden;">
<div id="<?php echo $unique_id; ?>" class="brid" itemprop="video" itemscope itemtype="http://schema.org/VideoObject"></div>
<script type="text/javascript">$bp("<?php echo $unique_id; ?>",<?php echo json_encode($options); ?>);</script>
</body>
</html>
<?php
}catch(Exception $e){
	//handle exception
	echo $e->getMessage();
}
?>